package com.dh.full.stack.persona.service.model.repositories;

import com.dh.full.stack.persona.service.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
