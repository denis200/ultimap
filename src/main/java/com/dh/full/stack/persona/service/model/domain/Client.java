package com.dh.full.stack.persona.service.model.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Client_Table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "clientid",referencedColumnName = "personid")
})
public class Client extends Person{
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastpurchase", nullable = false, updatable = false)
    private Date lastPurchase;

    public Date getLastPurchase() {
        return lastPurchase;
    }

    public void setLastPurchase(Date lastPurchase) {
        this.lastPurchase = lastPurchase;
    }
}
