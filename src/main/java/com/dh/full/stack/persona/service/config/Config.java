package com.dh.full.stack.persona.service.config;

import com.dh.full.stack.persona.service.bean.Asus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class Config {
    @Bean
    @Scope("singleton")
    public Asus beanAsus() {
        Asus asus = new Asus();
        asus.setName("I am  Asus");
        return asus;
    }
}
