package com.dh.full.stack.persona.service.model.repositories;

import com.dh.full.stack.persona.service.model.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client,Long> {
}
