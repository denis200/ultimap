package com.dh.full.stack.persona.service.controller;

import com.dh.full.stack.persona.service.input.EmployeeCreateInput;
import com.dh.full.stack.persona.service.model.domain.Employee;
import com.dh.full.stack.persona.service.service.EmployeeCreateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import io.swagger.annotations.Api;

@Api(
        tags = "employee",
        description = "Operations over employees "
)
@RestController
@RequestMapping("/public/employees")
@RequestScope
public class EmployeeController {

    @Autowired
    private EmployeeCreateService employeeCreateService; //AccountRepository accountRepository;
    @ApiOperation(
            value="Create an Employee"
    )
    @ApiResponses(
            {
                    @ApiResponse(
                            code=401,
                            message = "UnAuthorized to create employee"
                    )

            }
    )
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput input){

        employeeCreateService.setInput(input);
     employeeCreateService.execute();
     //
        employeeCreateService.save();
//
        return employeeCreateService.getEmployee();//getemployee




        // Account account = new Account();
        // account.setEmail(input.getEmail());
        //account.setState(input.getState());

        //return accountRepository.save(account);
    }
}
