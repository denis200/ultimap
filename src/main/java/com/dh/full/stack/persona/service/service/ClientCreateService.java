package com.dh.full.stack.persona.service.service;

import com.dh.full.stack.persona.service.input.ClientCreateInput;
import com.dh.full.stack.persona.service.model.domain.Client;
import com.dh.full.stack.persona.service.model.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

@Scope("prototype")
@Service

public class ClientCreateService {
    private ClientCreateInput input;

    @Autowired
    private ClientRepository clientRepository;

    private Client client;

    public void execute(){
        Client clientInstance = composeClientInstance();//(account);
        client = clientRepository.save(clientInstance);

    }

    private Client composeClientInstance(){//(Account account){
        Client instance = new Client();
        instance.setLastPurchase(new Date());
        instance.setEmail(input.getEmail());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setMale(input.getMale());
        instance.setDeleted(Boolean.TRUE);
        instance.setCreatedDate(new Date());

        return instance;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setInput(ClientCreateInput input) {
        this.input = input;
    }
}
