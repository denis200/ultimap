package com.dh.full.stack.persona.service.model.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "detail_table")

public class Detail {
    @Id
    @Column(name = "detailid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "totalproducts",length=100,nullable = false)
    private Integer totalProducts;

    @Column(name = "totalprice", nullable = false)
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long totalPrice;

   @OneToOne(fetch = FetchType.LAZY, optional = false) //@ManyToOne
    @JoinColumn(name = "detailsaleid", referencedColumnName = "saleid", nullable = false)
    private Sale sale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Sale getSale() {return sale;}

    public void setSale(Sale sale) {this.sale = sale;}
}
