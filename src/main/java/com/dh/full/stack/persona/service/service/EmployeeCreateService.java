package com.dh.full.stack.persona.service.service;

import com.dh.full.stack.persona.service.config.EmployeeProperties;
import com.dh.full.stack.persona.service.input.EmployeeCreateInput;
import com.dh.full.stack.persona.service.model.domain.Employee;
import com.dh.full.stack.persona.service.model.domain.Male;
import com.dh.full.stack.persona.service.model.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

@Scope("prototype")
@Service
public class EmployeeCreateService {
    private EmployeeCreateInput input;

    @Autowired
    private EmployeeRepository employeeRepository;
     @Autowired
     private EmployeeProperties employeeProperties;
/////


    public Employee save(){

        System.out.println(employeeProperties.getNombre());
        System.out.println(employeeProperties.getApellido());

        return employeeRepository.save(composeEmployeeInstance());

    }



///



    private Employee employee;

    public void execute(){
        Employee employeeInstance = composeEmployeeInstance();//(account);
        employee = employeeRepository.save(employeeInstance);

    }

    private Employee composeEmployeeInstance(){
//        Male mali = Male.MASCULINO;
//        Boolean isPermit = isPermit();
//        if(isPermit){mali=Male.FEMENINO;}

        Employee instance = new Employee();
        instance.setPosition(input.getPosition());
        instance.setEmail(input.getEmail());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setMale(input.getMale());
        instance.setDeleted(Boolean.TRUE);
        instance.setCreatedDate(new Date());

        //instance.setPassword(input.getPassword());
        return instance;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }



    public void setInput(EmployeeCreateInput input) {
        this.input = input;
    }
}

