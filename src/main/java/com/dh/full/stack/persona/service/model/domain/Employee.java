package com.dh.full.stack.persona.service.model.domain;

import javax.persistence.*;

@Entity
@Table(name = "employee_Table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid",
                referencedColumnName = "personid")
})
public class Employee extends Person{
    //@Column(name ="user_id",nullable = false)
    //j//private Long userId;
    @Column(name = "position", nullable = false)
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
