package com.dh.full.stack.persona.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.service.Contact;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("users-api") //employees-api
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.dh.full.stack.persona.service.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);

    }
    private ApiInfo apiEndPointsInfo () {
        return new ApiInfoBuilder()
                .title("Persons Service API")
                .description("Users Management REST API ")
                .contact(new Contact("denisO", "", "momento_triste@hotmail.com"))
                .version("0.0.1")  //0.0.2

                .license("Apache 1.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .build();
    }
}

