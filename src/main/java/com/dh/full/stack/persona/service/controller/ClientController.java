package com.dh.full.stack.persona.service.controller;

import com.dh.full.stack.persona.service.input.ClientCreateInput;
import com.dh.full.stack.persona.service.model.domain.Client;
import com.dh.full.stack.persona.service.service.ClientCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

@Api(
        tags = "client",
        description = "Operations over clients "
)
@RestController
@RequestMapping("/public/clients")
@RequestScope

public class ClientController {
    @Autowired
    private ClientCreateService clientCreateService;
    @ApiOperation(
            value="Create an Client"
    )
    @ApiResponses(
            {
                    @ApiResponse(
                            code=401,
                            message = "UnAuthorized to create client"
                    )

            }
    )
    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody ClientCreateInput input){

        clientCreateService.setInput(input);
        clientCreateService.execute();

        return clientCreateService.getClient();

    }
}
