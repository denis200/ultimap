package com.dh.full.stack.persona.service.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration

public class EmployeeProperties {
//    @Value("${employee.permit.age:18}")
//    private Integer permitAge;
//    public Integer getPermitAge() {
//        return permitAge;
//    }
    @Value ("${nombre}")
    private String nombre;
    @Value ("${apellido}")
    private String apellido;

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
}
