package com.dh.full.stack.persona.service.model.domain;

import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "person_table")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Person {
    @Id
    @Column(name = "personid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)  //ENTITY
    private Long id;

    @Column(name = "email",length=100,nullable = false)
    private String email;

    @Column(name = "firstname", length = 50, nullable = false)
    private String firstName;
    @Column(name = "lastname", length = 50, nullable = false)
    private String lastName;

    @Enumerated(EnumType.STRING)
    //@Column(name = "state", length = 20, nullable = false)
    private Male male;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "isdeleted", nullable = false)
    private Boolean isDeleted;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createddate", nullable = true, updatable = false)
    private Date createdDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Male getMale() {
        return male;
    }

    public void setMale(Male male) {
        this.male = male;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
