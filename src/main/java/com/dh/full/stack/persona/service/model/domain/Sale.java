package com.dh.full.stack.persona.service.model.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sale_table")
public class Sale {
    @Id
    @Column(name = "saleid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    //@Id
    @Column(name = "numbersale", nullable = false)
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long numberSale;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createddate", nullable = false, updatable = false)
    private Date createdDate;


    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "salesemployeid",referencedColumnName = "personid")
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "salesclientid",referencedColumnName = "personid")
    private Client client;
     ///
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }



    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
