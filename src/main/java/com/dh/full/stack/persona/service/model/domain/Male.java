package com.dh.full.stack.persona.service.model.domain;

public enum Male {
    MASCULINO,
    FEMENINO
}
